package r2m

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"time"

	mqtt "github.com/eclipse/paho.mqtt.golang"
)

var (
	defaultPublishTimeout = 20 * time.Second
)

type Handler struct {
	mqttc mqtt.Client
	topic string
}

var _ http.Handler = &Handler{}

func NewHandler(topic string, mc mqtt.Client) http.Handler {
	return &Handler{
		mqttc: mc,
		topic: topic,
	}
}

func (h *Handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}

	defer func() {
		io.Copy(ioutil.Discard, r.Body)
		r.Body.Close()
	}()

	var msg, topicOverride string
	var err error
	switch r.Header.Get("Content-Type") {
	case "application/json":
		msg, topicOverride, err = handleJson(r.Body)
	}

	topic := h.topic
	if topicOverride != "" {
		topic = topicOverride
	}

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
	}

	token := h.mqttc.Publish(topic, 1, false, msg)
	if !token.WaitTimeout(defaultPublishTimeout) {
		w.WriteHeader(http.StatusBadGateway)
		w.Write([]byte("timed out publishing to mqtt"))
		return
	}

	err = token.Error()
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(fmt.Sprintf("error publishing: %s", err)))
		return
	}

	w.WriteHeader(http.StatusCreated)
}

func handleJson(r io.Reader) (string, string, error) {
	msg := struct {
		Msg   string `json:"msg"`
		Topic string `json:"topic_override"`
	}{}

	err := json.NewDecoder(r).Decode(&msg)
	if err != nil {
		return "", "", err
	}
	return msg.Msg, msg.Topic, nil
}
