all: bin

bin:
	go build -o build/r2md github.com/ralfonso/r2m/cmd/r2md

clean:
	rm build/*

image: bin
	docker build -t ralfonso/r2m -f containers/Dockerfile .

push: image
	docker push ralfonso/r2m
