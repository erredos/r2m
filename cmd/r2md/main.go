package main

import (
	"crypto/tls"
	"crypto/x509"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"time"

	mqtt "github.com/eclipse/paho.mqtt.golang"
	"github.com/gorilla/handlers"
	"github.com/ralfonso/r2m"
	kingpin "gopkg.in/alecthomas/kingpin.v2"
)

var (
	app      = kingpin.New("r2md", "rest to mqtt bridge")
	httpAddr = app.Flag("http.addr", "http server listen addr").
			Envar("R2MD_HTTP_ADDR").Required().TCP()
	mqttServerAddr = app.Flag("mqtt.server.addr", "mqtt server addr").
			Envar("R2MD_MQTT_SERVER_ADDR").Required().URL()
	mqttTopic = app.Flag("mqtt.topic", "mqtt topic").
			Envar("R2MD_MQTT_TOPIC").Required().String()
	mqttClientCertFile = app.Flag("mqtt.client.cert", "mqtt client cert file").
				Envar("R2MD_MQTT_CLIENT_CERT_FILE").String()
	mqttClientKeyFile = app.Flag("mqtt.client.key", "mqtt client key file").
				Envar("R2MD_MQTT_CLIENT_KEY_FILE").String()
	mqttClientCACertFile = app.Flag("mqtt.ca.cert", "mqtt ca cert file").
				Envar("R2MD_MQTT_CA_CERT_FILE").String()
)

func main() {
	kingpin.MustParse(app.Parse(os.Args[1:]))

	log.Printf("connecting to mqtt server, %s", *mqttServerAddr)
	mc := mustMQTTClient()
	handler := r2m.NewHandler(*mqttTopic, mc)
	handler = handlers.LoggingHandler(os.Stdout, handler)
	listenAddr := *httpAddr
	log.Printf("starting http server, %s", listenAddr.String())
	log.Fatal(http.ListenAndServe(listenAddr.String(), handler))
}

func mustMQTTClient() mqtt.Client {
	brokerAddr := *mqttServerAddr
	co := mqtt.NewClientOptions()
	co = co.AddBroker(brokerAddr.String())

	tc := &tls.Config{}
	if *mqttClientCertFile != "" {
		// Load client cert
		cert, err := tls.LoadX509KeyPair(*mqttClientCertFile, *mqttClientKeyFile)
		if err != nil {
			log.Fatalf("error loading cert/key, %v", err)
		}

		// Load CA cert
		caCert, err := ioutil.ReadFile(*mqttClientCACertFile)
		if err != nil {
			log.Fatalf("error loading CA cert, %v", err)
		}
		caCertPool := x509.NewCertPool()
		caCertPool.AppendCertsFromPEM(caCert)

		// Setup HTTPS client
		tc.Certificates = []tls.Certificate{cert}
		tc.RootCAs = caCertPool
		tc.BuildNameToCertificate()
	}

	co = co.SetTLSConfig(tc)
	cl := mqtt.NewClient(co)
	token := cl.Connect()

	if !token.WaitTimeout(5 * time.Second) {
		log.Fatal("timed out connecting to mqtt server")
	}

	err := token.Error()
	if err != nil {
		log.Fatalf("error connecting to mqtt server, %v", err)
	}

	return cl
}
